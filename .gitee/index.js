const app = getApp()
const fetch = app.fetch
Page({
  data: {
    swiper: ['https://6865-heshuai1758876793-vuz52-1302722872.tcb.qcloud.la/banner_1.png?sign=a56c9c279a82192a16342da229f2533e&t=1599311129','https://6865-heshuai1758876793-vuz52-1302722872.tcb.qcloud.la/banner_2.png?sign=5e271addeafdae3efa61d77c6cb53b35&t=1599311157',
  'https://6865-heshuai1758876793-vuz52-1302722872.tcb.qcloud.la/banner_3.png?sign=6a33e8f88d744bbd8b73f6af5952064b&t=1599311195'],
    ad: 'https://6865-heshuai1758876793-vuz52-1302722872.tcb.qcloud.la/image_ad.png?sign=a4f655418ae0660c4d50780ba96386db&t=1599311225',
    category: ['https://6865-heshuai1758876793-vuz52-1302722872.tcb.qcloud.la/bottom_1.png?sign=6f07fb8208d312ea6bd5fcd5c7e7574f&t=1599311249','https://6865-heshuai1758876793-vuz52-1302722872.tcb.qcloud.la/bottom_2.png?sign=b07423313fcb543b4372504451969d61&t=1599311274','https://6865-heshuai1758876793-vuz52-1302722872.tcb.qcloud.la/bottom_3.png?sign=977c1099a8d87e8a52d803cefd6c04a9&t=1599311296','https://6865-heshuai1758876793-vuz52-1302722872.tcb.qcloud.la/bottom_4.png?sign=ef7dbfac92bd9e3931b9dcda949b1fef&t=1599311331']
  },
  onLaunch:function(){
    wx.cloud.init({
      env:"heshuai1758876793-vuz52"
    })
  },
  onLoad: function (options) {
    var callback = () => {
      wx.showLoading({
        title: '努力加载中',
        mask: true
      })
      fetch('food/index').then(data => {
        wx.hideLoading()
        this.setData({
       //   swiper: data.img_swiper,
       //   ad: data.img_ad,
       //   category: data.img_category
        })
      }, () => {
        callback()
      })
    }
    if (app.userInfoReady) {
      callback()
    } else {
      app.userInfoReadyCallback = callback
    }
  },
  start: function () {
    wx.navigateTo({
      url: '/pages/list/list',
    })
  }
})